# introduction
A `netflow` collector to calcuate user bandwith usage of selected websites(or IPs).

This collector has an admin interface which you can add websites(for example facebook.com). The collector then traces IPs(of your network) that are 
connecting to the target websites and calcuate their bandwidth usage.

There's a REST api(written in Express.js) that gives an external API access
to the program's database.

# installation 
## installation on Ubuntu 16.04

### mongodb

install mongo db

```
$ sudo apt-get install mongodb
```

make a ramdisk

to make ramdisk add the following to the `/etc/fstab`
```
tmpfs   /media/ramdisk tmpfs rw,size=1500M 0 0
```

to change database path for to ramdisk change `/etc/mongodb.conf`:

```
dbpath=/media/ramdisk
nojournal=true
noprealloc=true
```

###  `nodejs`

```
$ curl -sL https://deb.nodesource.com/setup_6.x -o nodesource_setup.sh
$ sudo bash nodesource_setup.sh
$ sudo apt-get install nodejs
$ sudo apt-get install build-essential
```

## making boot time services
for running the `collector` and `webserver` services copy `faraflow.service`
and `faraserver.service` to `/etc/systemd/system` directory.

```
$ sudo cp faraflow.service  /etc/systemd/system
$ sudo cp faraserver.service /etc/systemd/system
$ sudo systemctl enable faraflow.service
$ sudo systemctl enable faraserver.service
```

change the paths in service files to the location of the service files.

## starting the services

```
$ sudo service faraflow start
$ sudo service faraserver start
```