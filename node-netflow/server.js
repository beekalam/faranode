var express = require('express');
var cluster = require('cluster');
var path = require('path');
var jade = require('jade');
var mongodb = require("mongodb");
var Sequelize = require('sequelize');
var MongoClient =require("mongodb").MongoClient;
var bodyParser = require("body-parser");
var cookieParser = require("cookie-parser");
var session = require("express-session");
var RedisStore = require('connect-redis')(session);
var captchapng = require('captchapng');

function get_ip(req)
{
	var reqip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
	if (reqip.substr(0, 7) == "::ffff:") {
  		reqip = reqip.substr(7);
	}
	return reqip;
}
var url="mongodb://127.0.0.1:27017/netflow";
//sqlite datase path
var db_path  = path.join(__dirname,"db.sqlite");
var port = 2000;
var worker_count = 3;
var allowed_ips = ['94.74.138.14','94.74.128.19','94.74,128.22','94.74.128.15','94.74.128.14','109.203.185.251','172.16.22.12'];
if(cluster.isMaster)
{

	for(var i=0;i < worker_count; ++i){
		cluster.fork();
	}

	cluster.on('online',function(worker){
		console.log('Worker ' + worker.process.pid + ' is online.');
	});

	cluster.on('exit',function(worker,code,signal){
		console.log('Worker ' + worker.process.pid + ' died with code: ' + code + ', and signal: ' + signal);
		console.log('Starging new worker');
		cluster.fork();
	});

}else
{
var app = express();
app.use(express.static('vendor'));
app.set('views',path.join(__dirname,'views'));
app.set('view engine', 'jade');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(session({
			store: new RedisStore({host:"127.0.0.1",port:"6379"}),
			secret: '2C44774A-D649-4D44-9535-46E296EF984F', 
			resave:false, 
			saveUninitialized:true,
  			cookie: { maxAge: 3600000 } }));
app.use(cookieParser('3CCC4ACD-6ED1-4844-9217-82131BDCB239'));

// app.locals.pretty = true;
// app.set('json spaces', 4);
var authorize = function(req, res, next){
	if(req.session && req.session.username){
		return next();
	}else{
		res.redirect("/admin-login");
	}
}

//////////////////////////////////////////////////////////////////////////////
app.get('/old_index',authorize,function(req, res){
	
	var filter_ips;
	var sequelize = new Sequelize({
    	dialect: 'sqlite',
    	storage: db_path
  	});
  	sequelize.query("select * from filters order by address",{type:sequelize.QueryTypes.SELECT}).then(results => {
  		MongoClient.connect(url, function(err, database){
			if(err) throw err;
	    	database.collection("usage").find().toArray(function(err, items){
	    		for(var i=0; i < results.length; ++i){
	    			for(var j=0; j< items.length;++j){
	    				results[i]["download"]=results[i]["upload"] = results[i]["total_bytes"] = 0;
	    				if(items[j]["ip"] == results[i]["ip"]){
	    					results[i]["download"] = items[j]["download"];
	    					results[i]["upload"] = items[j]["upload"];
	    					results[i]["total_bytes"] = items[j]["total_bytes"];
	    					break;
 	    				}
	    			}
	    		}

				res.render('index',{title:'Home',ips:results});
				database.close()
			});
		});
  	});	

});


app.get('/',authorize,function(req, res){
	
	var filter_ips;
	var sequelize = new Sequelize({
    	dialect: 'sqlite',
    	storage: db_path
  	});
  	sequelize.query("select * from filters order by address",{type:sequelize.QueryTypes.SELECT}).then(results => {
  		MongoClient.connect(url, function(err, database){
			if(err) throw err;
	    	database.collection("usage").find().toArray(function(err, items){
	    		for(var i=0; i < results.length; ++i){
	    			for(var j=0; j< items.length;++j){
	    				results[i]["download"]=results[i]["upload"] = results[i]["total_bytes"] = 0;
	    				if(items[j]["ip"] == results[i]["ip"]){
	    					results[i]["download"] = items[j]["download"];
	    					results[i]["upload"] = items[j]["upload"];
	    					results[i]["total_bytes"] = items[j]["total_bytes"];
	    					break;
 	    				}
	    			}
	    		}

				res.render('index2',{title:'Home',ips:results});
				database.close()
			});
		});
  	});	

});

app.get('/data',authorize,function(req, res){
	var offset = req.query.offset ? req.query.offset : 0;
	var limit = req.query.limit ? req.query.limit : 0;
	var order = req.query.order ? req.query.order: 'asc';
	var search = req.query.search ? req.query.search : "";
	var sort = req.query.sort ? req.query.sort:"";
	var order = req.query.order ? req.query.order:"";
	var search = req.query.search ? req.query.search:"";

	var filter_ips;
	var sequelize = new Sequelize({
    	dialect: 'sqlite',
    	storage: db_path
  	});
  	console.log(req.query.limit);
  	console.log("++++++++++++++++++++++++++++++");
  	console.log(req.query);

  	var search_query = "",order_query="", paging=" limit " + limit + " offset " + offset;
  	if(search!="")
  		 search_query = " where  ( address like '%" + search + "%' or ip like '%" + search + "%' or persian_comment like '%" + search + "%')";

  	if(sort!="" && order !="")
  		order_query += " order by " + sort + " " + order;

  	
  	var query = "select * from filters " + search_query + order_query + paging;
  	console.log("in data query: ");
  	console.log(query);

  	sequelize.query("select count(*) as total from filters" + search_query).then(countresult =>{
	  	sequelize.query(query,{type:sequelize.QueryTypes.SELECT}).then(results => {
	  		MongoClient.connect(url, function(err, database){
				if(err) throw err;
				console.log("_____________________________");
				console.log(countresult);
				console.log(countresult[0][0]["total"]);
		    	database.collection("usage").find().toArray(function(err, items){
		    		for(var i=0; i < results.length; ++i){
		    			for(var j=0; j< items.length;++j){
		    				results[i]["download"]=results[i]["upload"] = results[i]["total_bytes"] = 0;
		    				if(items[j]["ip"] == results[i]["ip"]){
		    					results[i]["download"] = items[j]["download"];
		    					results[i]["upload"] = items[j]["upload"];
		    					results[i]["total_bytes"] = items[j]["total_bytes"];
		    					break;
	 	    				}
		    			}
		    		}
					res.set({'Content-Type': 'application/json; charset=utf-8'}).status(200).send({"rows":results,"total":countresult[0][0].total});
					// res.render('index2',{title:'Home',ips:results});
					// res.send()
					database.close()
				});
			});
	  	});	
  	});

});
// var filter=["79.127.127.35","79.127.127.8","79.127.127.21","185.49.85.21"];
///////////////////////////////////////////////////////
app.post('/add-ip',authorize, function(req, res){
	
	var ip = req.body.ip;
	var address = req.body.address;
	var comment = req.body.comment;
	var persian_comment = req.body.persianComment;
	var show_in_site = req.body.show_in_site;

	console.log([ip, comment, persian_comment,address,show_in_site]);

	var sequelize = new Sequelize({
    	dialect: 'sqlite',
    	storage: db_path
  	});
  	sequelize.query("select * from filters where ip='" + ip + "'",{Type:sequelize.QueryTypes.SELECT}).then(results => {
  		if(results[0] && results[0].length > 0)
  		{
  			res.end(JSON.stringify({"success":false,"msg":"duplicate ip"}));
  		}else{
  			 	var cmd = "insert into filters(ip,address,comment,persian_comment,show_in_site)values('" 
  			 				+ ip + "','" + address + "','" + comment + "','" + persian_comment + "','" + show_in_site + "')";
			  	sequelize.query(cmd,{type:sequelize.QueryTypes.INSERT}).then(results => {
			  		res.end(JSON.stringify({"success":true}));
			    	// res.render("index",{title:'Home',ips:results});
			  	});
  		}
  	});
 
	// MongoClient.connect(url, function(err, database){
	// 	if(err) throw err;
	// 	database.collection("filter_ips").insertOne({"ip":ip,"comment":""},function(err, data){
	// 		if(err) throw err;
	// 		// res.end({success:"true",msg:"success"});
	// 		res.end("successfuly");
	// 		database.close()
	// 	});
	// });
});
app.post('/edit-ip',authorize,function(req, res){
	
	var id = req.body.id;
	var ip = req.body.ip;
	var address = req.body.address;
	var comment = req.body.comment;
	var persian_comment = req.body.persianComment;
	console.log([id,ip, comment, persian_comment,address]);

	var sequelize = new Sequelize({
    	dialect: 'sqlite',
    	storage: db_path
  	});
  	sequelize.query("select * from filters where (ip='" + ip + "' and id!=" + id + ")",{Type:sequelize.QueryTypes.SELECT}).then(results => {
  		if(results[0] && results[0].length > 0)
  		{
  			res.end(JSON.stringify({"success":false,"msg":"duplicate ip"}));
  		}else{
  			 	var cmd = "update filters set ip='" + ip + "', address='" + address + "', comment='" + comment + "', persian_comment='" + persian_comment + "' where id=" + id;
			  	sequelize.query(cmd,{type:sequelize.QueryTypes.UPDATE}).then(results => {
			  		res.end(JSON.stringify({"success":true}));
			    	// res.render("index",{title:'Home',ips:results});
			  	});
  		}
  	});
});

app.get("/captcha",function(req, res){
	var num = parseInt(Math.random()*9000+1000);
	var p = new captchapng(80,30,num); // width,height,numeric captcha 
    p.color(0, 0, 0, 0);  // First color: background (red, green, blue, alpha) 
    p.color(80, 80, 80, 255); // Second color: paint (red, green, blue, alpha) 

    var img = p.getBase64();
    var imgbase64 = new Buffer(img,'base64');
    res.writeHead(200, {'Content-Type': 'image/png'});
    req.session.captcha = num;
    res.end(imgbase64);
});
///////////////////////////////////////////////////////
app.post('/remove-ip/:id', authorize,function(req, res){
	
	var id = req.params.id;

	var sequelize = new Sequelize({
    	dialect: 'sqlite',
    	storage: db_path
  	});
  	sequelize.query("delete from filters where id=" + id + "",{Type:sequelize.QueryTypes.SELECT}).then(results => {
  		res.end(JSON.stringify({success:true}));
  	});
	// MongoClient.connect(url, function(err, database){
	// 	if(err) throw err;
	// 	database.collection("filter_ips").remove({"_id":new mongodb.ObjectID(id)},function(err, data){
	// 		if(err) throw err;
	// 		// res.end({success:"true",msg:"success"});
	// 		res.end("successfuly");
	// 		database.close();
	// 	});
	// });
});

app.get('/cleardb',authorize, function(req, res){
		
	MongoClient.connect(url,function(err,database){
		if(err) throw err;
		database.collection("flows").remove({},function(err, result){
			if(err) throw err;
			res.set({'Content-Type': 'application/json; charset=utf-8'}).status(200).send({success:true});
			database.close();
		});
	});
})

app.post("/update-site-status", authorize,function(req,res){
	var id = req.body.id;
	var show = req.body.show == 1 ? 1 : 0;

	var sequelize = new Sequelize({
		dialect: 'sqlite',
		storage: db_path
  	});

  	sequelize.query("select * from filters where id=" + id ,{Type:sequelize.QueryTypes.SELECT}).then(results => {
  		if(results[0] && results[0].length == 0)
  		{
  			res.end(JSON.stringify({"success":false,"msg":"invalid id"}));
  		}else{
		 	var cmd = "update filters set show_in_site=" + show + " where id=" + id;
		  	sequelize.query(cmd,{type:sequelize.QueryTypes.UPDATE}).then(results => {
		  		res.end(JSON.stringify({"success":true}));
		  	});
  		}
  	});

});
///////////////////////////////////////////////////////////////
app.get("/admin-login",function(req, res){
	var to_send = {};
	if(req.session.error){
		to_send.error = req.session.error;
		delete req.session.error;
	}
	res.render('admin-login',to_send);
});

app.post("/admin-login",function(req, res){
	if(!req.body.username || !req.body.password){
		return res.render('admin-login',{error:'Please enter username and password'});
	}
	if(!req.body.captcha || req.session.captcha != req.body.captcha){
		console.log("req body captcha")
		console.log(req.body.captcha);
		console.log("req session captcha");
		console.log(req.session.captcha);
		return res.render("admin-login",{error:"Wrong captcha"});
	}
	var sequelize = new Sequelize({
    	dialect: 'sqlite',
    	storage: db_path
  	});
  	var cmd = "select * from users where (username='" + req.body.username + "' and password='" + req.body.password + "')";
  	sequelize.query(cmd,{Type:sequelize.QueryTypes.SELECT}).then(results => {
  		
  		if(results[0] && results[0].length > 0){
  			req.session.username = req.body.username;
  			req.session.save(function(err){
  				if(err) throw err;
  				res.redirect("/");
  			});
  		}else{
  			req.session.username="";
  			req.session.error="wrong username or password";
  			res.redirect("/admin-login");
  		}
  	});


});
app.get("/admin-logout",function(req, res){
	req.session.destroy(function(){
		res.redirect("/admin-login");
	});
});
///////////////////////////////////////////////////////////////
app.get("/login/:loginip",function(req, res){
	// var reqip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
	// if (reqip.substr(0, 7) == "::ffff:") {
 //  		reqip = reqip.substr(7);
	// }
	// if(allowed_ips.indexOf(reqip)==-1)
	// {
	// 	res.end("...");
	// }

	
	var loginip = req.params.loginip;
	//TODO: dismiss invalid ips e.g: 192.168.*
	// if(loginip.startsWith("192.168.")){
	// 	res.end(JSON.stringify{success:true});
	// }
	
	MongoClient.connect(url,function(err, database){
		if(err) throw err;
		var findByIp = loginip.split("___")[0] + "___";
		// var findByIp = "192.168.0.36"+"___";
		// console.log("find ip: " + findByIp);
		database.collection("flows").remove({"ip":{$regex:"^" + findByIp}},function(err, data){
			if(err) throw err;
			// console.log("tried to delete previous records");
			database.collection("flows").update({"ip":{$regex:loginip}},{"total_bytes":0,"ip":loginip,"download":0,"upload":0},{upsert:true},function(err, result){
				if(err) {
					res.end(JSON.stringify({success:false,msg:"error updating"}));
					throw err;
				}
				res.end(JSON.stringify({success:true}));
				database.close();
			});
		});

    });
});


app.get('/flows',function(req, res){
	// var reqip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
	// if (reqip.substr(0, 7) == "::ffff:") {
 //  		reqip = reqip.substr(7);
	// } 
	// if(allowed_ips.indexOf(reqip)==-1)
	// {
	// 	res.end("...");
	// }

	// console.log(req.connection.remoteAddress);
	console.log(	(Date.now()).toString()	);
	MongoClient.connect(url,function(err,database){
		if(err) throw err;
		database.collection("flows").find({},{_id:0,ip:1,total_bytes:1,upload:1,download:1}).toArray(function(err, data){
			if(err){ 
				re.send({success:false,msg:"error reading flows from db"}); 
			}
			res.set({'Content-Type': 'application/json; charset=utf-8'}).status(200).send(data);
			console.log(	(Date.now()).toString()	);
			database.close();
		});
	});


});


app.get("/persiansites",authorize,function(req, res){
	var sequelize = new Sequelize({
    	dialect: 'sqlite',
    	storage: db_path
  	});
  	console.log(req.connection.remoteAddress);
  	sequelize.query("select * from filters group by address order by id",{Type:sequelize.QueryTypes.SELECT}).then(results => {
  		// res.end("<pre>" + JSON.stringify(results[0]) + "</pre>");
  		res.render("persiansites",{title:'Home',vm:results[0]});
  	});
});

app.get("/persian-sites",function(req, res){
	var sequelize = new Sequelize({
    	dialect: 'sqlite',
    	storage: db_path
  	});
  	sequelize.query("select * from filters where show_in_site=1 group by address order by id",{Type:sequelize.QueryTypes.SELECT}).then(results => {
  		// res.end("<pre>" + JSON.stringify(results[0]) + "</pre>");
  		// res.render("persiansites",{title:'Home',vm:results[0]});
  		res.set({'Content-Type':'application/json; charset=utf-8'}).status(200).send(results[0]);
  	});
});


app.get("/user-summary",authorize,function(req,res){
	MongoClient.connect(url,function(err,database){
		if(err) throw err;
		database.collection("flows").find({},{_id:0,ip:1,total_bytes:1,upload:1,download:1}).toArray(function(err, data){
			if(err){ 
				re.send({success:false,msg:"error reading flows from db"}); 
			}
			res.render("user-summary",{"vm": data});
			database.close();
		});
	});
});

app.get("/net-summary",authorize,function(req, res){
	MongoClient.connect(url,function(err,database){
		if(err) throw err;
		database.collection("flows").find({},{_id:0,ip:1,total_bytes:1,upload:1,download:1}).toArray(function(err, data){
			if(err){ 
				re.send({success:false,msg:"error reading flows from db"}); 
			}
			res.render("net-summary",{"vm": data});
			database.close();
		});
	});
});

app.listen(port, function(){
	console.log('The server is running at http://localhost:%s',port);
});

}//end else