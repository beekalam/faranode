# starting the webserver & collector

	$ npm start
	$ npm stop

# starting the webserver & collector manually

	$ node server
	$ node collector

# database configuration

## configuring MongoDB in ramdisk

create a ramdisk
	
	$ mount -t tmpfs -o size=1000MB /media/ramdisk

give access to user `mongodb`

	$ chown mongodb /media/ramdisk

Change the `/etc/monodb.conf` configurtion

	dbpath=/media/ramdisk
	nojournal=true
	noprealloc = true

restart the `MongoDB` service

	$ service mongodb restart

# auto mounting ramdisk at startup

add the following line at the end of fstab(this is for Ubuntu).
make sure you have created the directory `ramdisk`.

tmpfs	/media/ramdisk	tmpfs rw,size=1000M 0 0