const Sequelize = require('sequelize');
const path = require('path');
const db_path  = path.join(__dirname,"db.sqlite");


function get_filter_ips()
{

  const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: db_path
  });  
  sequelize.query("select * from filters",{type:sequelize.QueryTypes.SELECT}).then(results => {
    console.log(results);
    return results;
  });

}

// console.log(get_filter_ips());

function insert_test()
{

  const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: db_path
  });  
  
  for(var i=0; i< 100;++i){
    var ip="192.168.1." +i;
    var comment = "some comment" + i;
    var cmd = "insert into filters(ip,comment)values('" + ip + "','" + comment + "')";
    sequelize.query(cmd,{type:sequelize.QueryTypes.INSERT}).then(results => {
       console.log(results);
    });
  }

}


function delete_test()
{

  const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: db_path
  });  
  
    var cmd = "delete from filters";
    sequelize.query(cmd,{type:sequelize.QueryTypes.DELETE}).then(results => {
       console.log(results);
    });

}

get_filter_ips();
// delete_test();
// insert_test();