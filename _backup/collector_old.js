const cluster 	= require('cluster');
const http 		= require('http');
var path 		= require("path");
const numCPUs 	= require('os').cpus().length;
var Collector 	= require('node-netflowv9');
var MongoClient = require("mongodb").MongoClient;
var ObjectId 	= require("mongodb").ObjectId;
var Sequelize 	= require("sequelize");

function numberWithComma(i)
{
	return i.toString().replace(/\B(?=(\d{3})+(?!\d))/g,",");
}
////////////////////////////////////////////////////////////////////////////
var url 				 =	"mongodb://127.0.0.1:27017/netflow";
var db_path 			 = 	path.join(__dirname,"db.sqlite");
var db_reread_interval   =  300;  //seconds							   
var in_bytes 			 = 	0;
var packet_count		 =	0;
var workers			 	 =	[];
var filters 			 = 	new Map();
var netflow_ip			 = '0.0.0.0';
var netflow_port		 = 2055;
////////////////////////////////////////////////////////////////////////////
if (cluster.isMaster) {
 	console.log(`Master ${process.pid} is running`);

 	console.log("Loading filters from DB.");
	var sequelize = new Sequelize({dialect:'sqlite', storage:db_path});
	sequelize.query("select * from filters", {Type:sequelize.QueryTypes.SELECT}).then(results => {
		for(var j=0; j < results[0].length;++j)
		{
			filters.set(results[0][j]["ip"],"");
		}
		console.log("fites size: " + filters.size);

		for (var i = 0; i < numCPUs; i++) {
	 		workers[i] = cluster.fork();
		}
		sequelize.close();

		MongoClient.connect(url,function(err, database){
			if(err) throw err;
			// database.collection("flows").remove({},function(err, res){
			// 	console.log("Creating flows collection.");
			// });

			database.collection("flows").createIndex({ip:1},function(err, res){
				console.log("Creating index on flows collection");
				database.close();
			});

			setInterval(() => {
				console.log('...................................................');
				console.log(`in master[${process.pid}] `);
				var sequelize = new Sequelize({
			    	dialect: 'sqlite',
			    	storage: db_path
			  	});
			  	sequelize.query("select * from filters",{Type:sequelize.QueryTypes.SELECT}).then(results => {
			  		if(results[0])
			  		{
			  			var entries_change = false;
			  			var data = results[0];
			  			//check if ips added
			  			for(var i=0; i < data.length;++i)
			  			{
			  				if(!filters.has(data[i]["ip"]))
			  				{
			  					entries_change = true;break;
			  				}
			  			}
			  			//check if ip was deleted
			  			filters.forEach(function(value,key,map){
			  				var found = false;
			  				for(var i=0; i< data.length; ++i){
			  					if(data[i]["ip"] == key){
			  						found = true;break;
			  					}
			  				}
			  				if(!found){
			  					entries_change = true;
			  				}
						});
						if(entries_change)
						{
							//update master filters entry
							filters.clear();
							for(var i=0; i < data.length;++i)
				  			{
				  				filters.set(data[i]["ip"],"");
				  			}
				  			//send update notice to children
							var msg ={ "type":"entriesChanged", data:data};
							for(var id in cluster.workers){
								cluster.workers[id].send(JSON.stringify(msg));
							}
						}

			  		}
			  		sequelize.close();
			  	});
			},db_reread_interval * 1000);
			
		});
	
	  cluster.on('exit', (worker, code, signal) => {
	    console.log(`worker ${worker.process.pid} died`);
	  });
	  
	});

} 
else {
		
	var db,i;
	var m = new Map();

	process.on('message',function(msg){
		//console.log("new msg: " + msg);
		msg = JSON.parse(msg);
		if(msg["type"] == "entriesChanged")
		{
			filters.clear();
			var data = msg["data"];
			for(var j=0; j < data.length; ++j){
				filters.set(data[j]["ip"],"");
			}		
			// console.log("new filters loaded.");
			//console.log("filters size: " + filters.size);
		}
	});

	var sequelize = new Sequelize({dialect:'sqlite', storage:db_path});
	sequelize.query("select * from filters", {Type:sequelize.QueryTypes.SELECT}).then(results => {
		MongoClient.connect(url,function(err, database){
			if(err) throw err;
			db =  database;
		});
		for(var j=0; j < results[0].length;++j)
		{
			filters.set(results[0][j]["ip"],"");
		}
	
		// console.log("++++++++++++++++++++++++");
		// filters.forEach(function(value,key,map){
		// 		console.log(key);
		// });

		// console.log("=================================");
		// console.log(filters.size);
		// console.log("=================================");
		sequelize.close();

		Collector({port:netflow_port,host:netflow_ip,cb:function(flow){
			
			for(i=0;i < flow.flows.length;++i)
			{
				 // console.log(flow.flows[i]);
				// if(filter.indexOf(flow.flows[i].ipv4_src_addr) !== -1) // || filter.indexOf(packet.ipv4_dst_addr) !== -1)
				// if(testfilter.indexOf(packet.ipv4_src_addr) !== -1 || testfilter.indexOf(packet.ipv4_dst_addr) !== -1)

				if(filters.has(flow.flows[i].ipv4_src_addr))
				{
					// insertOne(packet.ipv4_src_addr , packet.ipv4_dst_addr,new Date(flow["header"]["seconds"] * 1000), packet.in_bytes,db);
					// insertOne(packet.ipv4_src_addr , packet.ipv4_dst_addr, flow["header"]["seconds"], packet.in_bytes,db);
					// cache.push({ ip:flow.flows[i].ipv4_src_addr,  total_bytes:flow.flows[i].in_bytes});
					// console.log("download from: " + flow.flows[i].ipv4_src_addr);
					if(m.has(flow.flows[i].ipv4_dst_addr))
					{
						m.get(flow.flows[i].ipv4_dst_addr).total_bytes += flow.flows[i].in_bytes;
						m.get(flow.flows[i].ipv4_dst_addr).download    += flow.flows[i].in_bytes;
					}else{
						m.set(flow.flows[i].ipv4_dst_addr, {ip:flow.flows[i].ipv4_dst_addr,total_bytes:flow.flows[i].in_bytes,download:flow.flows[i].in_bytes,upload:0});
					}
				// }else if(filter.indexOf(flow.flows[i].ipv4_dst_addr) !== -1){
				}else if(filters.has(flow.flows[i].ipv4_dst_addr)){
					// console.log("upload to: " + flow.flows[i].ipv4_dst_addr);
					if(m.has(flow.flows[i].ipv4_src_addr))
					{
						m.get(flow.flows[i].ipv4_src_addr).total_bytes += flow.flows[i].in_bytes;
						m.get(flow.flows[i].ipv4_src_addr).upload	   += flow.flows[i].in_bytes;
					}else{
						m.set(flow.flows[i].ipv4_src_addr, {ip:flow.flows[i].ipv4_src_addr,total_bytes:flow.flows[i].in_bytes,download:0,upload:flow.flows[i].in_bytes});
					}
				}
			}

			if(m.size > 20)
			{
				m.forEach(function(value,key,map){
					// console.log(value);
					// console.log(`m[${key}] = ${value}`);
					db.collection("flows").update({"ip":{$regex:'^'+ value.ip + '___'}},{$inc:{"total_bytes":value.total_bytes,"download":value.download,"upload":value.upload}});
					// db.collection("flows").update({"ip":value.ip},{"ip":value.ip,$inc:{"total_bytes":value.total_bytes,"download":value.download,"upload":value.upload}},{upsert:true});
					// db.collection("flows").insertOne({"ip":value.ip+"___","total_bytes":value.total_bytes});
				});
				// for(var j=0; j< cache.length; ++j)
				// {
				// 	db.collection("flows").update({"ip":{$regex:'^'+ cache[j].ip +'$'}},{$inc:{"total_bytes":cache[j].total_bytes}});
				// 	// db.collection("testflows").insert({"ip":cache[j].ip, "total_bytes":cache[j].total_bytes});
				// }
				m.clear();
			}

		}});
	});

	console.log(`Worker ${process.pid} started`);
}
