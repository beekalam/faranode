var express = require('express');
var path = require('path');
var jade = require('jade');
var mongodb = require("mongodb");
var Sequelize = require('sequelize');
var MongoClient =require("mongodb").MongoClient;

var url="mongodb://127.0.0.1:27017/netflow";
//sqlite datase path
var db_path  = path.join(__dirname,"db.sqlite");
var port = 2000;

var app = express();
app.use(express.static('vendor'));
app.set('views',path.join(__dirname,'views'));
app.set('view engine', 'jade');
app.locals.pretty = true;
app.set('json spaces', 4);
//////////////////////////////////////////////////////
app.get('/',function(req, res){
	var filter_ips;
	var sequelize = new Sequelize({
    	dialect: 'sqlite',
    	storage: db_path
  	});  
  	sequelize.query("select * from filters",{type:sequelize.QueryTypes.SELECT}).then(results => {
    	res.render("index",{title:'Home',ips:results});
  	});
	// MongoClient.connect(url, function(err, database){
	// 	if(err) throw err;
	//     database.collection("filter_ips").find().toArray(function(err, items){
	//     	// console.log(items);
	//     	filter_ips = items;
	// 		res.render('index',{title:'Home',ips:filter_ips});
	// 	});
	// });
	
});
// var filter=["79.127.127.35","79.127.127.8","79.127.127.21","185.49.85.21"];
///////////////////////////////////////////////////////
app.post('/add-ip/:ip/:comment', function(req, res){
	var ip = req.params.ip;
	var comment = req.params.comment;
	var sequelize = new Sequelize({
    	dialect: 'sqlite',
    	storage: db_path
  	});
  	sequelize.query("select * from filters where ip='" + ip + "'",{Type:sequelize.QueryTypes.SELECT}).then(results => {
  		console.log(results);
  		if(results[0] && results[0].length > 0)
  		{
  			console.log("in if");
  			res.end(JSON.stringify({"success":false,"msg":"duplicate ip"}));
  		}else{
  			 	var cmd = "insert into filters(ip,comment)values('" + ip + "','" + comment + "')";
			  	sequelize.query(cmd,{type:sequelize.QueryTypes.INSERT}).then(results => {
			  		res.end(JSON.stringify({"success":true}));
			    	// res.render("index",{title:'Home',ips:results});
			  	});
  		}
  	});
 
	// MongoClient.connect(url, function(err, database){
	// 	if(err) throw err;
	// 	database.collection("filter_ips").insertOne({"ip":ip,"comment":""},function(err, data){
	// 		if(err) throw err;
	// 		// res.end({success:"true",msg:"success"});
	// 		res.end("successfuly");
	// 		database.close()
	// 	});
	// });
});
///////////////////////////////////////////////////////
app.post('/remove-ip/:id', function(req, res){
	var id = req.params.id;

	var sequelize = new Sequelize({
    	dialect: 'sqlite',
    	storage: db_path
  	});
  	sequelize.query("delete from filters where id=" + id + "",{Type:sequelize.QueryTypes.SELECT}).then(results => {
  		console.log(results);
  		res.end(JSON.stringify({success:true}));
  	});
	// MongoClient.connect(url, function(err, database){
	// 	if(err) throw err;
	// 	database.collection("filter_ips").remove({"_id":new mongodb.ObjectID(id)},function(err, data){
	// 		if(err) throw err;
	// 		// res.end({success:"true",msg:"success"});
	// 		res.end("successfuly");
	// 		database.close();
	// 	});
	// });
});
///////////////////////////////////////////////////////
app.get('/usage/:from/:to/:addr',function(req, res){
	// var from = 1512152593;
	// var to= 1512152600;
	// var addr = "192.168.0.83";

	var from = parseInt(req.params.from);
	var to = parseInt(req.params.to);
	var addr = req.params.addr;
	console.log(from, to, addr);
	
	MongoClient.connect(url,function(err, database){
		if(err) throw err;
		var q = { $and:[{'ts':{$gte: from, $lte: to}}, {$or:[{'src_addr':addr},{'dst_addr':addr}]}] };
		var query = [
					{$match:
						{ $and:[{'ts':{$gte: from, $lte: to}}, {$or:[{'src_addr':addr},{'dst_addr':addr}]}] }
					},
					{$group:
						{_id:null, "total_bytes":{$sum: "$total_bytes"}}
					}];
		// console.log(query);
		database.collection("flows").aggregate(query).toArray(function(err, data){
		// database.collection("flows").find(q).toArray(function(err, data){
		// database.collection("flows").find().toArray(function(err, data){
			if(err) throw err;
			// res.send("got data from collection");
			// console.log(data);
			// res.send(query);
			res.end(JSON.stringify(data));
			// console.log(data);
			database.close();
		});
	});

});

app.get('/usage/:addr',function(req, res){
	// var addr = "192.168.0.83";

	var addr = req.params.addr;
	
	
	MongoClient.connect(url,function(err, database){
		if(err) throw err;
		var query = [
					{$match:
						 {$or:[{'src_addr':addr},{'dst_addr':addr}]}
					},
					{$group:
						{_id:null, "total_bytes":{$sum: "$total_bytes"}}
					}];
		database.collection("flows").aggregate(query,function(err, data){
			if(err) throw err;
			ret = JSON.stringify(data);
			database.collection("flows").remove({$or:[{'src_addr':addr},{'dst_addr':addr}]},function(err, data){
				if(err) throw err;
				ret += "cleared connections";
			});
			res.end(ret);
			database.close();
		});
	});

});

app.get('/cleardb', function(req, res){
	MongoClient.connect(url,function(err,database){
		if(err) throw err;
		database.collection("flows").remove({},function(err, database){
			if(err) throw err;
			res.send("cleared successfuly");
			database.close();
		});
	});
})



app.get('/test',function(req,res){

	// console.timeStamp("label");
	MongoClient.connect(url, function(err, database){
		if(err) throw err;
	    database.collection("flows").find().toArray(function(err, items){
	    	// console.log(items);
	    	// filter_ips = items;
	    	var a =[];
	    	var ipt="192.168.0.";
	    		for(var j=1; j<=254;++j)
	    		{
	    			a.push('"' +ipt +  j + '"');
	    		}
			res.end(a.join(","));
		});
	});
	// console.timeStamp("label");

});




app.get("/login/:loginip",function(req, res){
	var loginip = req.params.loginip;
	console.log("in login");
	console.log(loginip);

	MongoClient.connect(url,function(err, database){
		if(err) throw err;
		var findByIp = loginip.split("___")[0] + "___";
		// var findByIp = "192.168.0.36"+"___";
		console.log("find ip: " + findByIp);
		database.collection("flows").remove({"ip":{$regex:"^" + findByIp}},function(err, data){
			if(err) throw err;
			console.log("tried to delete previous records");
			database.collection("flows").update({"ip":{$regex:loginip}},{"total_bytes":0,"ip":loginip,"download":0,"upload":0},{upsert:true},function(err, result){
				if(err) {
					res.end(JSON.stringify({success:false,msg:"error updating"}));
					throw err;
				}
				res.end(JSON.stringify({success:true}));
				database.close();
			});
		});

    });
});


app.get('/flows',function(req, res){

	MongoClient.connect(url,function(err,database){
		if(err) throw err;
		var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
		console.log(ip);
		database.collection("flows").find({},{_id:0,ip:1,total_bytes:1,upload:1,download:1}).toArray(function(err, data){
			if(err){ re.send(JSON.stringify({success:false,msg:"error reading flows from db"})); }
			res.set({'Content-Type': 'application/json; charset=utf-8'}).send(200,JSON.stringify(data));
			// console.log(data);
			database.close();
		});
	});

});


// app.get('/flows',function(req, res){
// 	MongoClient.connect(url,function(err,database){
// 		if(err) throw err;
// 		database.collection("flows").count({},function (req, res) {
// 			console.log(res);
// 		});
// 		console.log(count);
// 	});
// });
app.listen(port, function(){
	console.log('The server is running at http://localhost:%s',port);
});
