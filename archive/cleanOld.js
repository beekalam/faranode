var MongoClient =require("mongodb").MongoClient;
var ObjectID = require("mongodb").ObjectID;
var ObjectId = require("mongodb").ObjectId;
var url="mongodb://127.0.0.1:27017/netflow";

// var objectId = ObjectID.createFromTime(Date.now() *  1000 - 24 * 60 * 60);
var objectId = ObjectID.createFromTime(Date.now()   - 24 * 60 * 60 * 1000);
// console.log(objectId);
// var objectId = ObjectID.createFromTime(Date.now()	);
// var objectId = ObjectID.createFromTime(1);
// console.log(objectId);

var objectIdFromDate = function (date) {
	return Math.floor(date.getTime() / 1000).toString(16) + "0000000000000000";
};

var dateFromObjectId = function (objectId) {
	return new Date(parseInt(objectId.substring(0, 8), 16) * 1000);
};

MongoClient.connect(url,function(err, database){
	var from = new Date();
	from.setHours(from.getHours() - 20);
	
	database.collection("flows").find({_id:{$lt: ObjectId(objectIdFromDate(from))}}).limit(40000).toArray(function(err, items){
		for(var i=0;i < items.length;++i)
		{
			database.collection("flows").remove({"_id":items[i]["_id"]},function(err,data){
				if(err) throw err;
				// console.log("cleaned log");
			});
		}
		database.close();	
	});

});
