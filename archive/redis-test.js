var redis = require('redis');
const cluster = require('cluster');
const http = require('http');
const numCPUs = require('os').cpus().length;
var Collector = require('node-netflowv9');
var MongoClient =require("mongodb").MongoClient;
var client = redis.createClient();

//////////////////////////////////////////////////////////////////////
client.on('error', function(err){
  console.log('Something went wrong ', err)
});

client.set('my test key', 'my test value', redis.print);
client.get('my test key', function(error, result) {
  if (error) throw error;
  console.log('GET result ->', result)
});

//////////////////////////////////////////////////////////////////////
var url="mongodb://127.0.0.1:27017/netflow";

var in_bytes = 0, packet_count=0;
var s1 = -1;
var cache=[];
var workers=[];
var current_worker=0;
		
var db,i;
var cache = [];
MongoClient.connect(url,function(err, database){
	if(err) throw err;
	console.log("creating connection.....");
	db =  database;
});

function updatekey(key,client,new_value){
	// console.log("update key.....");
	client.keys("*" + key +"__*",function(err, data){
			if (err) throw err;
			// console.log(data);
			if(data.length===1){
				// console.log(data[0]);
				client.incrby(data[0],new_value);
			}
			// }else{
				// console.log("does not exist")
				// client.incrby(cache[j].src_addr+"__" + "logintime" + "__shit",cache[j].total_bytes);
			// }
	});
}

Collector({port:2055,host:'0.0.0.0',cb:function(flow){
	
	for(i=0;i < flow.flows.length;++i)
	{
		packet = flow.flows[i];

		//if(filter.indexOf(packet.ipv4_src_addr) !== -1 || filter.indexOf(packet.ipv4_dst_addr) !== -1)
		{
			in_bytes += packet.in_bytes;
			// insertOne(packet.ipv4_src_addr , packet.ipv4_dst_addr,new Date(flow["header"]["seconds"] * 1000), packet.in_bytes,db);
			// insertOne(packet.ipv4_src_addr , packet.ipv4_dst_addr, flow["header"]["seconds"], packet.in_bytes,db);
			cache.push({ src_addr:packet.ipv4_src_addr, dst_addr:packet.ipv4_dst_addr,ts:flow["header"]["seconds"], total_bytes:packet.in_bytes});
		}
	}
	
	if(cache.length > 200)
	{
		// console.log("cache size: " + cache.length);
		for(var j=0; j < cache.length; ++j)
		{
			updatekey(cache[j].src_addr, client, cache[j].total_bytes);
			// var key_exists = false;
			// client.incrby(cache[j].src_addr+"__" + "logintime" + "__shit",cache[j].total_bytes);

			//client.incrby(cache[j].dst_addr,cache[j].total_bytes);
			// if(client.exists(cache[j].src_addr))
			// {
			// 	// client.set(cache[j].src_addr,cache[i].total_bytes);
			// 	// client.set(cache[j].src_addr,client.get(cache[j].src_addr) + cache[j].total_bytes);
			// 	//console.log("key exists: " + cache[j].src_addr + " value: " + client.get(cache[j].src_addr));
			// }else
			// {
			// 	console.log("setting new key");
			// 	client.set(cache[j].src_addr, cache[j].total_bytes);
			// }
		}
		client.quit();
		cache=[];
		in_bytes = 0;
	}

	if(packet_count++ % 1000 == 0)
	{
	 // console.log("#" + packet_count + " (" + cluster.worker.id + ") : " +  numberWithComma(in_bytes));	
	}

}});
